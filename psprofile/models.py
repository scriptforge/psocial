from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from django.utils import timezone
import os


# Specifies the full path where we should upload avatars to
# TODO: determine file type, save as username.png, etc..
# TODO: confirm it's an image of the correct dimensions
def account_avatar_filename(instance, filename):
    return os.path.join('psprofile', instance.domain, instance.username, filename)


class Account(models.Model):
    """
    Accounts, both local and remote
    """
    username = models.CharField(max_length=128)
    domain = models.CharField(max_length=128)
    is_local = models.BooleanField()
    user = models.ForeignKey(User, null=True, blank=True, default=None)
    uri = models.CharField(max_length=191, blank=True)  # needed?
    url = models.URLField(null=True, blank=True)
    name = models.CharField(max_length=191)
    bio = models.TextField(default='', blank=True)
    location = models.CharField(max_length=200, blank=True, default='Planet Earth')
    website = models.URLField(max_length=400, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)
    avatar = models.ImageField(max_length=200, upload_to=account_avatar_filename, blank=True)
    # TODO: Resize and crop avatar within web app
    salmon_url = models.URLField(blank=True)
    timeline_feed = models.URLField(blank=True)
    hub_url = models.URLField(blank=True)
    tags = TaggableManager(blank=True)

    class Meta:
        verbose_name = "Local and Remote Account"
        verbose_name_plural = "Local and Remote Accounts"

    def __str__(self):
        return self.username + '@' + self.domain


class Follow(models.Model):
    """
    List of accounts and who they are following
    """
    account = models.ForeignKey(Account)
    target_account = models.ForeignKey(Account, related_name='account_target_account')
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)

    class Meta:
        verbose_name = "Follow"
        verbose_name_plural = "Follows"

    def __str__(self):
        return self.account.username + '@' + self.account.domain + ' : ' + \
               self.target_account.username + '@' + self.target_account.domain
