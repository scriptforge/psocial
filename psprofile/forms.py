import re
from django.utils.translation import ugettext as _
from django import forms
from django.core.validators import validate_email, URLValidator, ValidationError

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions


def get_sendto_choices():
    return ((1, "Everyone"),)

def validate_resource(value):
    """
    Validates the OStatus resource as a http/https or user@domain
    """
    valid = False
    try:
        validate_email(value)
        return
    except ValidationError as v_email_error:
        pass

    try:
        validate = URLValidator()
        validate(value)
        return
    except ValidationError as v_url_error:
        pass

    # Failed both verifications, raise our own.
    raise ValidationError(_("Resource must be a http:// or https:// or user@domain"))


class RemoteFollowForm(forms.Form):
    resource = forms.CharField(
        label='User',
        widget=forms.TextInput,
        validators=[validate_resource]
    )

    def __init__(self, *args, **kwargs):
        super(RemoteFollowForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.form_action = '/profile/remote_follow'
        self.helper.form_method = 'get'
        self.helper.layout = Layout(
            Field('resource', css_class='input-xlarge'),
            FormActions(
                Submit('remote_follow', 'Follow', css_class='btn-primary')
            )
        )
