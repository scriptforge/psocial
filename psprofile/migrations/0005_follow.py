# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-20 10:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('psprofile', '0004_auto_20160320_2131'),
    ]

    operations = [
        migrations.CreateModel(
            name='Follow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='psprofile.Account')),
                ('target_account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='account_target_account', to='psprofile.Account')),
            ],
            options={
                'verbose_name': 'Follow',
                'verbose_name_plural': 'Follows',
            },
        ),
    ]
