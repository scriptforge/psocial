from urllib import request
from xml.dom import minidom, Node
import os
import re
import json
import feedparser

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

from psprofile.models import Account
from pssite.models import Site
from salmon.models import SalmonAccountKey

def parse_atomsvc_feeds(feed_url):
    feeds = {}
    feed_descriptor = request.urlopen(feed_url, timeout=5)
    # TODO probably shouldn't use minidom for this because of XML Vulnerabilities
    xmldoc = minidom.parseString(feed_descriptor.read())
    collections = xmldoc.getElementsByTagName('collection')
    for collection in collections:
        url = collection.attributes['href'].value
        # Go looking for what type of stream it is
        for cn in collection.childNodes:
            if cn.nodeType == Node.ELEMENT_NODE and cn.tagName == 'atom:title':
                if 'timeline' in cn.childNodes[0].nodeValue:
                    feeds['timeline'] = url
                elif 'subscriptions' in cn.childNodes[0].nodeValue:
                    feeds['subscriptions'] = url
                elif 'favourites' in cn.childNodes[0].nodeValue:
                    feeds['favourites'] = url
                elif 'memberships' in cn.childNodes[0].nodeValue:
                    feeds['memberships'] = url
    return feeds

def collect_remote_follow_data(resource):
    """
    This grabs the latest remote account data from another social instance and updates our database record
    or creates a new one.
    """
    # Lookup the resource
    remote_resource = resource
    if re.match('http', resource) is None:
        # it's an user@domain, add an 'acct:' to the start of it
        remote_resource = 'acct:' + resource
        remote_server = re.match('.*@(.*)', remote_resource).group(1)
    else:
        try:
            remote_server = re.match('https?://(.*)/.*', resource).group(1)
        except:
            return None

    # Make the webfinger request to find the OStatus links
    webfinger_request = remote_server + '/.well-known/webfinger?resource=' + remote_resource
    try:
        # Try HTTPS
        response = request.urlopen('https://' + webfinger_request, timeout=5)
        jrd = json.loads(response.read().decode('utf-8'))
    except Exception as e:
            response = request.urlopen('http://' + webfinger_request, timeout=5)
            jrd = json.loads(response.read().decode('utf-8'))

    if not 'links' in jrd:
        raise Exception('Webfinger response has no links array')

    # Get the user and domain
    remote_user = ''
    remote_domain = ''
    if jrd['subject'].startswith('acct:'):
        try:
            parsed = re.match('acct:(.*?)@(.*)', jrd['subject'])
            remote_user = parsed.group(1)
            remote_domain = parsed.group(2)
            site = Site.objects.get(domain=remote_domain)
            if site is not None:
                # Local user, just return
                return
        except Site.DoesNotExist:
            # Not a local user, continue
            pass
    else:
        for a in jrd['aliases']:
            if a.startswith('acct'):
                try:
                    parsed = re.match('acct:(.*?)@(.*)', a)
                    remote_user = parsed.group(1)
                    remote_domain = parsed.group(2)
                    site = Site.objects.get(domain=remote_domain)
                    if site is not None:
                        # Local user, just return
                        return
                except Site.DoesNotExist:
                    # Not a local user, continue
                    pass

    # Find the links we need
    urls = {}
    for link in jrd['links']:
        if link['rel'] == 'http://apinamespace.org/atom' and link['type'] == 'application/atomsvc+xml':
            urls.update(parse_atomsvc_feeds(link['href']))
        if link['rel'] == 'salmon':
            urls['salmon'] = link['href']
        # This doesn't work - GNUSocial expects profile pages to be in https://site/user/ID so grab it from
        # The feed below instead
        if link['rel'] == 'http://webfinger.net/rel/profile-page':
            urls['profile'] = link['href']
        if link['rel'] == 'magic-public-key':
            urls['magic'] = link['href']

    # Now we have a bunch of feed URLs to parse
    # Atom timeline feed should be enough for now
    try:
        feed = feedparser.parse(urls['timeline'])['feed']

        # Grabs the profile page which will be correct for GNUSocial
        urls['profile'] = feed['author_detail']['href']

        remote_name = feed.get('poco_displayname', remote_user)
        remote_location = feed.get('poco_formatted', '')
        remote_website = feed.get('poco_value', '')
        remote_bio = feed.get('summary', '')
        remote_avatar_url = feed.get('logo', '')
        for link in feed['links']:
            if link['rel'] == 'hub':
                urls['hub'] = link['href']

        # Download the avatar URL, then we create the remote account record
        avatar_filename = os.path.basename(remote_avatar_url)
        avatar_tmp = NamedTemporaryFile(delete=True)
        avatar_tmp.write(request.urlopen(remote_avatar_url, timeout=5).read())
        avatar_tmp.flush()
    except:
        raise Exception('Could not collect remote user data')

    # Create or update account
    account, created = Account.objects.get_or_create(username=remote_user, domain=remote_domain, is_local=False)
    account.name = remote_name
    account.location = remote_location
    account.website = remote_website
    account.bio = remote_bio
    account.salmon_url = urls['salmon']
    account.timeline_feed = urls['timeline']
    account.hub_url = urls['hub']
    account.url = urls['profile']
    account.avatar.save(avatar_filename, File(avatar_tmp), save=True)
    account.save()

    # Create or update Salmon public key
    salmon, created = SalmonAccountKey.objects.get_or_create(account=account, public_key=SalmonAccountKey.get_public_key_from_magic(urls['magic']))
    salmon.save()

    return (account.username, account.domain)
