from django.conf.urls import patterns, url
from psprofile import views

urlpatterns = [
    url(r'^edit/(?P<username>[0-9A-Za-z]+)$', views.edit_profile, name='edit_profile'),
    url(r'^new$', views.new_profile, name='new_profile'),
    url(r'^follow/(?P<username>[0-9A-Za-z]+)$', views.local_follow, name='local_follow'),
    url(r'^(?P<username>[0-9A-Za-z]+)$', views.view_profile, name='view_profile'),
    url(r'^(?P<username>[0-9A-Za-z]+)/timeline/1.atom$', views.timeline_atom_feed, name='profile_timeline_atom_feed'),
    url(r'^(?P<username>[0-9A-Za-z]+?)/following', views.following_list, name='following_list'),
    url(r'^(?P<username>[0-9A-Za-z]+?)/followers', views.followers_list, name='followers_list'),
    url(r'^remote_follow$', views.remote_follow_request, name='remote_follow_request'),
    url(r'^remote_follow/(?P<username>[0-9A-Za-z]+?)@(?P<domain>[0-9A-Za-z.\-]+)', views.remote_follow_confirm, name='remote_follow_confirm'),
    url(r'^unfollow/(?P<username>[0-9A-Za-z]+?)@(?P<domain>[0-9A-Za-z.\-]+)', views.unfollow, name='unfollow'),
]
