from django.apps import AppConfig


class PsprofileConfig(AppConfig):
    name = 'psprofile'
    verbose_name = 'Profiles'
