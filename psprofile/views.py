import re

from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.urlresolvers import reverse

from pssite.models import Site
from psprofile.models import Account, Follow
from notice.models import Notice
from notice.forms import NoticeForm
from psprofile.forms import RemoteFollowForm
from psprofile.lib import collect_remote_follow_data
from pubsubhub.lib import push_subscribe
from salmon.lib import activitystream_follow, activitystream_unfollow

def new_profile(request):
    """
    Create a new profile
    """
    # TODO
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=username, domain=context['site'].domain)
    context['notice_form'] = NoticeForm()
    return render(request, "notice.html", context)


def edit_profile(request, username=None):
    """
    Edit the current user's profile
    """
    # TODO
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=username, domain=context['site'].domain)
    context['notice_form'] = NoticeForm()
    return render(request, "index.html")


@login_required
def local_follow(request, username=None):
    """
    Authenticated users can instantly follow other users by clicking the right button
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username,
                                                domain=context['site'].domain)
    profile_lookup = Account.objects.filter(username=username, domain=context['site'].domain)

    if (request.user.username == username):
        context['error_msg'] = "Cannot follow your own account"
        return render(request, "error.html", context)
    if len(profile_lookup) != 1:
        context['error_msg'] = "User Not Found"
        return render(request, "error.html", context)

    context['page_profile'] = profile_lookup[0]
    context['notice_form'] = NoticeForm()
    # See if they are already following
    follow, create = Follow.objects.get_or_create(account=context['profile'],
                                                  target_account=context['page_profile'])
    if not create:
        context['info_msg'] = 'Already following that user.'
    else:
        # Create a notice about following this user
        notice = Notice.objects.create(profile=context['profile'],
                                       content=context['profile'].name + ' started following ' +
                                               context['page_profile'].name + ' (' + context['page_profile'].username +
                                               '@' + context['page_profile'].domain,
                                       rendered=context['profile'].name + ' started following ' +
                                                context['page_profile'].name + ' (' + context['page_profile'].username +
                                                '@' + context['page_profile'].domain + ')',
                                       content_type='psocial/follow'
                                       )
        # Create a notice about the target getting followed
        notice = Notice.objects.create(profile=context['page_profile'],
                                       content=context['page_profile'].name + ' is being followed by ' +
                                               context['profile'].name + ' (' + context['profile'].username +
                                               '@' + context['profile'].domain,
                                       rendered=context['page_profile'].name + ' is being followed by ' +
                                                context['profile'].name + ' (' + context['profile'].username +
                                                '@' + context['profile'].domain + ')',
                                       content_type='psocial/followed'
                                       )
        # TODO Move the follow procedure into it's own function. Probably best done after remote follows feature is working
        # TODO Creating a notice should probably be it's own function too!

        # TODO This below is replicated in view_profile, grabbing the followers list and notices, need to implement redirects
        # Get list of users the account is following
        following = Follow.objects.filter(account=context['page_profile']).order_by('-updated_at')
        context['following'] = following

        # Get list of accounts following this user
        followers = Follow.objects.filter(target_account=context['page_profile']).order_by('-updated_at')
        context['followers'] = followers

        notices = Notice.objects.filter(profile=context['page_profile']).order_by('-updated_at')
        context['notices'] = notices

        context['info_msg'] = 'Started following ' + context['page_profile'].name

    # Return to the new profile we just followed
    return render(request, "profile.html", context)


def view_profile(request, username=None):
    """
    View a profile containing user details and timeline
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    # This is an anonymous view as well
    if request.user.is_authenticated():
        context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
        context['notice_form'] = NoticeForm()
        context['notice_form'].helper.form_action += '?next=' + reverse('view_profile', kwargs={'username': username})

    if username is None:
        messages.error(request, 'No user specified')
        return HttpResponseRedirect('home_view')

    try:
        profile_lookup = Account.objects.get(username=username, domain=context['site'].domain)
    except Account.DoesNotExist:
        messages.error(request, 'User Not Found')
        return HttpResponseRedirect(reverse('home_view'))

    context['page_profile'] = profile_lookup

    # Get list of users the account is following
    following = Follow.objects.filter(account=context['page_profile']).order_by('-updated_at')
    context['following'] = following

    # Get list of accounts following this user
    followers = Follow.objects.filter(target_account=context['page_profile']).order_by('-updated_at')
    context['followers'] = followers

    notices = Notice.objects.filter(profile=context['page_profile']).order_by('-updated_at')
    context['notices'] = notices

    return render(request, "profile.html", context)

@login_required
@require_http_methods(['GET'])
def remote_follow_confirm(request, username=None, domain=None):
    """
    Confirms a remote follow - adds a following link to the database
    """
    # TODO This should probably be fired and forget Celery task
    # TODO This does not actually ping the remote site to do the confirmation (yet)
    # TODO PubSubHub request to remote end
    # TODO Salmon ping to remote end
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    context['page_profile'] = context['profile']

    context['notice_form'] = NoticeForm()

    if username is None or domain is None:
        context['error_msg'] = 'Invalid user/domain to follow'
        return render(request, "remote_follow.html", context)

    try:
        remote_account = Account.objects.get(username=username, domain=domain)
    except Account.DoesNotExist:
        context['error_msg'] = 'Invalid user/domain to follow'
        return render(request, "remote_follow.html", context)

    follow, created = Follow.objects.get_or_create(account=context['profile'], target_account=remote_account)

    if created:
        context['info_msg'] = 'Started following user.'
        # Create a notice about following this user
        follow_notice = Notice.objects.create(profile=context['profile'],
                                       content=context['profile'].name + ' started following ' +
                                               remote_account.name + ' (' + remote_account.username +
                                               '@' + remote_account.domain + ')',
                                       rendered=context['profile'].name + ' started following ' +
                                                remote_account.name + ' (' + remote_account.username +
                                                '@' + remote_account.domain + ')',
                                       content_type='psocial/follow'
                                       )
        # Create a notice about the target getting followed
        followed_notice = Notice.objects.create(profile=remote_account,
                                       content=remote_account.name + ' is being followed by ' +
                                               context['profile'].name + ' (' + context['profile'].username +
                                               '@' + context['profile'].domain + ')',
                                       rendered=remote_account.name + ' is being followed by ' +
                                                context['profile'].name + ' (' + context['profile'].username +
                                                '@' + context['profile'].domain + ')',
                                       content_type='psocial/followed'
                                       )
        # TODO Move the follow procedure into it's own function. Probably best done after remote follows feature is working
        # TODO Creating a notice should probably be it's own function too!
        # This is replicated from the local_follow function above

        # Send PubSubHub Subscription Request
        push_subscribe(remote_account)
        # Send Salmon Atom Activity Stream to Follow
        activitystream_follow(context['profile'], remote_account, follow_notice)

    else:
        context['error_msg'] = 'Already following user.'

    # TODO This is replicated from the view profile function above but should probably be put in it's own function
    # TODO or use a redirect instead of a render
    # Get list of users the account is following
    following = Follow.objects.filter(account=context['page_profile']).order_by('-updated_at')
    context['following'] = following

    # Get list of accounts following this user
    followers = Follow.objects.filter(target_account=context['page_profile']).order_by('-updated_at')
    context['followers'] = followers

    notices = Notice.objects.filter(profile=context['page_profile']).order_by('-updated_at')
    context['notices'] = notices

    return render(request, "profile.html", context)


@login_required
@require_http_methods(['GET'])
def remote_follow_request(request):
    """
    Finds the remote OStatus user and presents a confirmation dialog
    """
    remote_resource = request.GET.get('resource', '')
    remote_confirm_subscribe = request.GET.get('confirm-subscribe', None)

    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    context['page_profile'] = context['profile']

    context['notice_form'] = NoticeForm()

    # If there is no resource specified, show the form instead
    if remote_resource == '':
        context['info_msg'] = 'Which remote user do you want to subscribe to?'
        context['remote_follow_form'] = RemoteFollowForm()
        return render(request, "remote_follow.html", context)

    # Validate the resource before looking it up
    remote_follow_form = RemoteFollowForm(request.GET)
    context['remote_follow_form'] = remote_follow_form
    if not remote_follow_form.is_valid():
        context['error_msg'] = 'Your resource is invalid.'
        return render(request, 'remote_follow.html', context)

    # TODO Should try to shortcut requests that are for local accounts
    try:
        r_username, r_domain = collect_remote_follow_data(remote_follow_form.cleaned_data['resource'])
    except:
        context['error_msg'] = 'Could not retrieve remote user data.'
        return render(request, 'remote_follow.html', context)

    remote_account = Account.objects.get(username=r_username, domain=r_domain)

    context['remote_account'] = remote_account

    # TODO This is replicated from the view profile function above but should probably be put in it's own function
    # TODO or use a redirect instead of a render
    # Get list of users the account is following
    following = Follow.objects.filter(account=context['page_profile']).order_by('-updated_at')
    context['following'] = following

    # Get list of accounts following this user
    followers = Follow.objects.filter(target_account=context['page_profile']).order_by('-updated_at')
    context['followers'] = followers

    # Display the result record to user with "Subscribe" (Confirm) button
    return render(request, 'remote_follow.html', context)

def timeline_atom_feed(request, username=None):
    """
    Generate ATOM 2.0 feed for this profile
    """
    if username is None:
        return HttpResponseBadRequest()

    content_type = 'application/atom+xml'
    context = {}
    context['site'] = Site.objects.all()[0]
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return HttpResponseBadRequest()

    context['account'] = Account.objects.get(user=user)
    #TODO No notices are in the feed yet
    context['notices'] = []

    return render(request, 'atom/profile_timeline.xml', context, content_type=content_type)

def following_list(request, username=None):
    """
    Show the list of accounts this user is following
    """
    next = request.GET.get('next', '/')

    context = {}
    context['site'] = Site.objects.all()[0]
    try:
        context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    except:
        messages.error(request, 'User does not exist')
        return HttpResponseRedirect(next)

    context['page_profile'] = context['profile']
    context['notice_form'] = NoticeForm()

    # TODO This is replicated from the view profile function above but should probably be put in it's own function
    # TODO or use a redirect instead of a render
    # Get list of users the account is following
    following = Follow.objects.filter(account=context['page_profile']).order_by('-updated_at')
    context['following'] = following

    # Get list of accounts following this user
    followers = Follow.objects.filter(target_account=context['page_profile']).order_by('-updated_at')
    context['followers'] = followers

    # Display the result record to user with "Subscribe" (Confirm) button
    return render(request, 'following.html', context)

def followers_list(request, username=None):
    """
    Show the list of accounts following this user
    """
    next = request.GET.get('next', '/')

    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    context['page_profile'] = context['profile']

    context['notice_form'] = NoticeForm()

    # TODO This is replicated from the view profile function above but should probably be put in it's own function
    # TODO or use a redirect instead of a render
    # Get list of users the account is following
    following = Follow.objects.filter(account=context['page_profile']).order_by('-updated_at')
    context['following'] = following

    # Get list of accounts following this user
    followers = Follow.objects.filter(target_account=context['page_profile']).order_by('-updated_at')
    context['followers'] = followers

    # Display the result record to user with "Subscribe" (Confirm) button
    return render(request, 'followers.html', context)


@login_required
@require_http_methods(['GET'])
def unfollow(request, username=None, domain=None):
    """
    Unfollow a user (be it local or remote)
    """
    next = request.GET.get('next', '/')

    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    context['page_profile'] = context['profile']

    context['notice_form'] = NoticeForm()

    # Look up the user to unfollow
    try:
        unfollow_account = Account.objects.get(username=username, domain=domain)
    except Account.DoesNotExist:
        messages.error(request, 'User does not exist')
        return HttpResponseRedirect(next)

    # Is it being followed?
    try:
        follow = Follow.objects.get(account=context['profile'], target_account=unfollow_account)
    except Follow.DoesNotExist:
        messages.error(request, 'You are not following that user')
        return HttpResponseRedirect(next)

    # Unfollow the account
    follow.delete()

    if not unfollow_account.is_local:
        # Create a temporary notice about the unfollow
        unfollow_notice = Notice.objects.create(profile=context['profile'],
                                              content=context['profile'].name + ' stopped following ' +
                                                      unfollow_account.name + ' (' + unfollow_account.username +
                                                      '@' + unfollow_account.domain + ')',
                                              rendered=context['profile'].name + ' stopped following ' +
                                                       unfollow_account.name + ' (' + unfollow_account.username +
                                                       '@' + unfollow_account.domain + ')',
                                              content_type='psocial/unfollow'
                                              )
        activitystream_unfollow(context['profile'], unfollow_account, unfollow_notice)

        # If this is the last local user following the remote user, then unsubscribe with PubSubHub as well
        unfollow_targets_remaining = Follow.objects.filter(target_account=unfollow_account)
        if len(unfollow_targets_remaining) == 0:
            pass

        # TODO The unfollow salmon and pubsubhub requests should get moved to Celery tasks as they will take time
        # Delete the temporary notice
        unfollow_notice.delete()

    messages.success(request, "User unfollowed")

    return HttpResponseRedirect(next)
