import re

from django.http import JsonResponse

from pssite.models import *

from webfinger.lib import webfinger_account_lookup


def webfinger(request):
    """
    This handles the well known webfinger request URL
    """
    resource = request.GET.get('resource', '')
    resp = {}
    # Parse the resource to find the username
    acctmatch = re.match('acct:(.*)@(.*)', resource)
    if acctmatch is not None:
        site = Site.objects.filter(domain=acctmatch.group(2))
        if site:
            # Match address with username, domain name
            resp = webfinger_account_lookup(acctmatch.group(1), resource)
    else:
        urlmatch = re.match('(https?://.*?)/(.*)', resource)
        if urlmatch is not None:
            # Match url with site name, then path
            site = Site.objects.filter(baseurl=urlmatch.group(1))
            if site:
                acct = urlmatch.group(2).split('/')[-1]
                resp = webfinger_account_lookup(acct, resource)
    # Return an empty JSON if not found otherwise return the response
    return JsonResponse(resp)
