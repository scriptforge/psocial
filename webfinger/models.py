from django.db import models
from django.contrib.auth.models import User


class Property(models.Model):
    property = models.CharField(max_length=300)
    value = models.CharField(max_length=300)

    class Meta:
        verbose_name = "Property"
        verbose_name_plural = "Properties"

    def __str__(self):
        return self.property


class Alias(models.Model):
    alias = models.CharField(max_length=300)

    class Meta:
        verbose_name = "Alias"
        verbose_name_plural = "Aliases"

    def __str__(self):
        return self.alias


class Link(models.Model):
    name = models.CharField(max_length=100, verbose_name="Name/Description")
    rel = models.CharField(max_length=300)
    href = models.CharField(max_length=300, blank=True)
    titles = models.CharField(max_length=300, blank=True)
    type = models.CharField(max_length=300, blank=True)
    properties = models.CharField(max_length=300, blank=True)

    class Meta:
        verbose_name = "Link"
        verbose_name_plural = "Links"

    def __str__(self):
        return self.name


class AccessControl(models.Model):
    user = models.ForeignKey(User)
    allowed_links = models.ManyToManyField(Link, blank=True)
    allowed_properties = models.ManyToManyField(Property, blank=True)

    class Meta:
        verbose_name = "Access Control"
        verbose_name_plural = "Access Controls"

    def __str__(self):
        return self.user.username

    # This lists all links for a user (used in the Admin page)
    def get_allowed_links(self):
        return '\n'.join([link.name for link in self.allowed_links.all()])

    # This lists all properties for a user (used in Admin page)
    def get_allowed_properties(self):
        return '\n'.join([prop.property for prop in self.allowed_properties.all()])
