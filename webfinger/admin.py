from django.contrib import admin
from webfinger.models import *


class LinkAdmin(admin.ModelAdmin):
    list_display = ('name', 'rel', 'type', 'href', 'titles', 'properties')


class AccessControlAdmin(admin.ModelAdmin):
    list_display = ('user', 'get_allowed_links', 'get_allowed_properties')


admin.site.register(Property)
admin.site.register(Alias)

admin.site.register(Link, LinkAdmin)

admin.site.register(AccessControl, AccessControlAdmin)
