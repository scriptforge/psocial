# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-09 08:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webfinger', '0005_auto_20160309_1942'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accesscontrol',
            name='allowed_links',
            field=models.ManyToManyField(blank=True, to='webfinger.Link'),
        ),
        migrations.AlterField(
            model_name='accesscontrol',
            name='allowed_properties',
            field=models.ManyToManyField(blank=True, to='webfinger.Property'),
        ),
    ]
