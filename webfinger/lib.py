import re

from django.contrib.auth.models import User
from webfinger.models import *
from psprofile.models import Account
from pssite.models import *
from salmon.models import SalmonAccountKey

"""
Does Jinja style replacements in fields, eg {{ username }}
"""


def render_webfinger_field(field, username, site, domain):
    field = re.sub('\{\{\s+username\s+\}\}', username, field)
    field = re.sub('\{\{\s+site\s+\}\}', site, field)
    field = re.sub('\{\{\s+domain\s+\}\}', domain, field)
    return field


"""
Perform webfinger on a user account
"""


def webfinger_account_lookup(username, resource):
    resp = {}
    user = User.objects.filter(username=username)
    # User must be active to get a response
    if user and user[0].is_active:
        # Site info
        sites = Site.objects.all()
        site = sites[0]

        # Fill in the JSON response based on that username
        # For now we will not bother with access controls
        # Might do away with them all together
        # Subject
        resp['subject'] = resource

        # Aliases
        # Take out the alias that the request asked for
        aliases = Alias.objects.all()
        resp['aliases'] = []
        for a in aliases:
            alias = render_webfinger_field(a.alias, username=username, site=site.baseurl, domain=site.domain)
            if alias != resource:
                resp['aliases'].append(alias)
        if len(resp['aliases']) == 0:
            resp.pop('aliases')

        # Properties
        # Properties are not used in the GNUSocial webfinger so this should be blank
        properties = Property.objects.all()
        resp['properties'] = []
        for p in properties:
            prop = {}
            property_name = render_webfinger_field(p.name, username=username, site=site.baseurl, domain=site.domain)
            property_value = render_webfinger_field(p.value, username=username, site=site.baseurl, domain=site.domain)
            prop['property_name'] = property_value
            resp['properties'].append(prop)
        if len(resp['properties']) == 0:
            resp.pop('properties')

        # Links
        links = Link.objects.all()
        resp['links'] = []
        for l in links:
            link = {}
            link['rel'] = l.rel
            # todo - add other fields, and check if field is blank or not
            if l.type != '':
                link['type'] = l.type
            if l.href != '':
                link['href'] = render_webfinger_field(l.href, username=username, site=site.baseurl, domain=site.domain)
            resp['links'].append(link)

        # Need to do magic-public-key lookup
        magic_public_key = SalmonAccountKey.get_magic_public_key(
            Account.objects.get(username=username, domain=site.domain))

        resp['links'].append({
                            'rel': 'magic-public-key',
                            'href': magic_public_key
                            })

        if len(resp['links']) == 0:
            resp.pop('links')
    return resp
