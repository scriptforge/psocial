from django.conf.urls import patterns, url
from webfinger import views

urlpatterns = [
    url(r'^$', views.webfinger, name='webfinger'),
]
