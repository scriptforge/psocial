from django.apps import AppConfig


class WebfingerConfig(AppConfig):
    name = 'webfinger'
