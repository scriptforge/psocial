from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest, HttpResponse
from django.views.decorators.http import require_http_methods

import pprint
import feedparser

from psprofile.models import Account, Follow
from pubsubhub.models import Subscription

@require_http_methods(['GET', 'POST'])
@csrf_exempt
def update_subscription(request, username=None, domain=None):
    """
    PUSH Subscription Callback URL
    """
    if username is None and domain is None:
        return HttpResponseBadRequest

    # Request is a subscription verification
    if request.method == 'GET':
        mode = request.GET.get('hub.mode', None)
        verify_token = request.GET.get('hub.verify_token', None)
        topic = request.GET.get('hub.topic', None)
        challenge = request.GET.get('hub.challenge', None)

        if mode is None or verify_token is None or topic is None or challenge is None:
            return HttpResponseBadRequest()

        # Look up the subscription, check the verify token
        try:
            account = Account.objects.get(username=username, domain=domain)
        except Account.DoesNotExist:
            return HttpResponseBadRequest()

        try:
            sub = Subscription.objects.get(account=account)
        except Subscription.DoesNotExist:
            return HttpResponseBadRequest()

        if sub.verify_token == verify_token:
            sub.verified = True
            sub.save()
            return HttpResponse(content=challenge, status=200)
        else:
            return HttpResponseBadRequest()

    # Request is an update
    if request.method == 'POST':
        # TODO Not implemented yet
        try:
            with open('/tmp/pprint.txt', 'w') as f:
                pp = pprint.PrettyPrinter(stream=f)
                feed = feedparser.parse(request.body)
                pp.pprint(feed)
            return HttpResponse()
        except:
            return HttpResponseBadRequest()

@require_http_methods(['GET', 'POST'])
@csrf_exempt
def remove_subscription(request, username=None, domain=None):
    """
    PUSH Unsubscription Callback URL
    """
    if username is None and domain is None:
        return HttpResponseBadRequest

    # Request is a subscription verification
    if request.method == 'GET':
        mode = request.GET.get('hub.mode', None)
        verify_token = request.GET.get('hub.verify_token', None)
        topic = request.GET.get('hub.topic', None)
        challenge = request.GET.get('hub.challenge', None)

        if mode is None or verify_token is None or topic is None or challenge is None:
            return HttpResponseBadRequest()

        # Look up the subscription, check the verify token
        try:
            account = Account.objects.get(username=username, domain=domain)
        except Account.DoesNotExist:
            return HttpResponseBadRequest()

        try:
            sub = Subscription.objects.get(account=account)
        except Subscription.DoesNotExist:
            return HttpResponseBadRequest()

        if sub.verify_token == verify_token:
            sub.verified = True
            sub.delete()
            return HttpResponse(content=challenge, status=200)
        else:
            return HttpResponseBadRequest()
