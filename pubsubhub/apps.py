from django.apps import AppConfig


class PubsubhubConfig(AppConfig):
    name = 'pubsubhub'
