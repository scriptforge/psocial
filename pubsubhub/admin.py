from django.contrib import admin

from pubsubhub.models import Subscription

admin.site.register(Subscription)