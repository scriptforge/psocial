from django.conf.urls import patterns, url
from pubsubhub import views

urlpatterns = [
    url(r'^update/(?P<username>[0-9A-Za-z]+?)@(?P<domain>[0-9A-Za-z.\-]+)',
        views.update_subscription, name='update_subscription'),
    url(r'^remove/(?P<username>[0-9A-Za-z]+?)@(?P<domain>[0-9A-Za-z.\-]+)',
        views.remove_subscription, name='remove_subscription'),
]
