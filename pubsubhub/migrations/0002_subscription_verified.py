# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-28 03:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pubsubhub', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='verified',
            field=models.BooleanField(default=False),
        ),
    ]
