from django.db import models
import random
import string

from psprofile.models import Account

class Subscription(models.Model):
    """
    PUSH Subscriptions for remote follows
    """
    account = models.OneToOneField(Account)
    verify_token = models.CharField(max_length=120)
    verified = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Subscription'
        verbose_name_plural = 'Subscriptions'

    def __str__(self):
        return self.account.username + '@' + self.account.domain

    @staticmethod
    def get_new_verify_token():
        """
        Verify tokens should be unique and hard to guess
        """
        return ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(64))
