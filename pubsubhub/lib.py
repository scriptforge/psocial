from urllib import request, parse, error

from pubsubhub.models import Subscription
from pssite.models import Site
def push_subscribe(account):
    """
    Makes a PubSubHub subscription request
    """
    site = Site.objects.all()[0]

    hub_request = {}
    hub_request['hub.mode'] = 'subscribe'
    hub_request['hub.verify'] = 'async'
    hub_request['hub.verify_token'] = Subscription.get_new_verify_token()
    hub_request['hub.topic'] = account.timeline_feed
    hub_request['hub.callback'] = site.baseurl + '/hub/update/' + account.username + '@' + account.domain

    # Create subscription record
    try:
        sub = Subscription.objects.get(account=account)
        # There already is a subscription
        return
    except Subscription.DoesNotExist:
        # We want there to be no subscription
        pass

    sub = Subscription.objects.create(account=account,
                                      verify_token=hub_request['hub.verify_token'],
                                      verified=False)
    sub.save()

    data = parse.urlencode(hub_request)
    data = data.encode('ascii')

    req = request.Request(account.hub_url, data)
    try:
        with request.urlopen(req, timeout=5) as response:
            resp_page = response.read()
        # Create subscription record

    except error.HTTPError:
        raise Exception('Unable to make PUSH subscription')

def push_unsubscribe(account):
    """
    Makes a PubSubHub unsubscription request
    """


    # Create subscription record
    try:
        sub = Subscription.objects.get(account=account)
    except Subscription.DoesNotExist:
        # There is no subscription to unsubscribe from
        raise Exception('No subscription to unsubscribe from')

    site = Site.objects.all()[0]

    hub_request = {}
    hub_request['hub.mode'] = 'unsubscribe'
    hub_request['hub.verify'] = 'async'
    hub_request['hub.verify_token'] = Subscription.get_new_verify_token()
    hub_request['hub.topic'] = account.timeline_feed
    hub_request['hub.callback'] = site.baseurl + '/hub/remove/' + account.username + '@' + account.domain

    sub.verify_token = hub_request['hub.verify_token']
    sub.verified = False
    sub.save()

    data = parse.urlencode(hub_request)
    data = data.encode('ascii')

    req = request.Request(account.hub_url, data)
    try:
        with request.urlopen(req, timeout=5) as response:
            resp_page = response.read()

    except error.HTTPError:
        raise Exception('Unable to make PUSH subscription')
