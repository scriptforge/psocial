#PSocial - Python/Django GNUSocial Compatible Social Site

## Website

Code: https://bitbucket.org/scriptforge/psocial

Demo/Dev instance: https://psocial.scriptforge.org

## About

PSocial is an experiment to develop a social media network site in Python and Django. It is attempting to be
compatible with existing GNUSocial implementations, and later will connect to other social network protocols.

PSocial implements the GNUSocial Web Application in Python3 and Django. The main motivations behind this implementation:
* I don't use PHP much but I use Python often
* Learn the Webfinger, PubSubHub and Salmon protocols behind OStatus by providing a Python implementation
* Improve Python skills, especially Python3 and Django (as my previous work has mostly been Python2)
* Implement and improve some best practices including some Agile, Continuous Integration
* (An additional implementation separates the protocol from the original StatusNet/GNUSocial application)

## Documentation

Right now there is none, except for the code.

## Contributing

Right now the app is a barely functioning prototype. There are no tests, and it works only so far as it has been tested
while coding for each feature. Many features could easliy break as there is minimal testing or exception handling. The
app is an experiment and should be used by no one.

That being said, the project is GPLv3 and pull requests are welcome. I manage development tasks via a Jira server,
which is currently set up private but this could be changed if there is

## Roadmap
v0.1.0 - Get a basic prototype working
v0.1.1 - Refactor the prototype and move to a continuous integration model
v0.2.0 - Implement additional federated OStatus features not completed in the basic prototype
v0.3.0 - Implement a REST API
v0.4.0 - Implement APIs to connect other clients (eg AndStatus),other Social networks (eg Pump.IO, Diaspora, Twitter)
v0.5.0 - User interface improvements/replacement/alternative (eg AJAX, themes, plugins other web framework, eg ReactJS)

## Contributors

* Scott Bragg (jsbragg@scriptforge.org) - https://scriptforge.org/faulteh

## Changelog

### Version 0.1.0 (3-Apr-2016)

* User can login
* Anonymous and user can view profiles and system timelines/activity feeds
* Can post public notices
* Can reply to notices
* Can favourite and unfavourite a notice
* Can repeat a notice
* Can follow and unfollow local and remote users
