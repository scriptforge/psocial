#!/bin/bash

NAME="psocial"                              #Name of the application (*)
DJANGODIR=/home/jsbragg/psocial             # Django project directory (*)
SOCKFILE=/home/jsbragg/psocial/run/gunicorn.sock        # we will communicate using this unix socket (*)
LOCALPORT=8000
USER=jsbragg                                        # the user to run as (*)
GROUP=jsbragg                                     # the group to run as (*)
NUM_WORKERS=1                                     # how many worker processes should Gunicorn spawn (*)
DJANGO_SETTINGS_MODULE=psocial.settings             # which settings file should Django use (*)
DJANGO_WSGI_MODULE=psocial.wsgi                     # WSGI module name (*)

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /home/jsbragg/psvenv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/jsbragg/psvenv/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user $USER \
  --bind=127.0.0.1:$LOCALPORT
