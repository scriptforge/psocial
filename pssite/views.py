from django.shortcuts import render
from django.core.urlresolvers import reverse


from .models import Site
from psprofile.models import Account
from notice.models import Notice
from notice.forms import NoticeForm

def home_view(request):
    """
    This serves as both the home page and the news feed page for a user when logged in
    Home page, when logged out, will show latest posts from all users on site
    Home page, when logged in, will show latest posts from all people the user is following
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    if request.user.is_authenticated():
        context['user'] = request.user
        context['profile'] = Account.objects.filter(user=request.user, domain=context['site'].domain)[0]
        context['notice_form'] = NoticeForm()
        context['notice_form'].helper.form_action += '?next=' + reverse('home_view')
        context['page_profile'] = context['profile']

        notices = Notice.objects.filter(profile=context['page_profile']).order_by('-updated_at')
        #TODO Add mentions to the list of notices
        context['notices'] = notices
    else:
        # User is not logged in, display list of notices related to local accounts on this instance
        local_accounts = Account.objects.filter(is_local=True)
        notices = Notice.objects.filter(profile=local_accounts).order_by('-updated_at')
        # Filter out 'psocial' content types (as these are only displayed on user profile pages)
        context['notices'] = notices
    return render(request, "index.html", context)
