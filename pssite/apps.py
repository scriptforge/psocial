from django.apps import AppConfig

class PssiteConfig(AppConfig):
    name = 'pssite'
    verbose_name = 'Site Configuration'
