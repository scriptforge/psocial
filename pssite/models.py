from django.db import models
from taggit.managers import TaggableManager

# Create your models here.
class Site(models.Model):
  baseurl = models.URLField(max_length=400)
  name = models.CharField(max_length=150)
  subtitle = models.CharField(max_length=250)
  domain = models.CharField(max_length=100)
  tags = TaggableManager()

  class Meta:
    verbose_name = "Site"
    verbose_name_plural = "Sites"
    
  def __str__(self):
    return self.baseurl

