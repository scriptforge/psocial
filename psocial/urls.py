"""psocial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

from pssite.views import home_view
from salmon.views import host_meta

admin.site.site_header = "PSocial Administration"

urlpatterns = [
                  url(r'^', include('django.contrib.auth.urls')),
                  url(r'^$', home_view, name='home_view'),
                  url(r'^admin/', admin.site.urls),
                  url(r'^.well-known/webfinger', include('webfinger.urls')),
                  url(r'^.well-known/host-meta$', host_meta, name='host_meta'),
                  url(r'^notice/', include('notice.urls')),
                  url(r'^profile/', include('psprofile.urls')),
                  url(r'^hub/', include('pubsubhub.urls'))
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
