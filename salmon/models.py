import base64

from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Util.number import long_to_bytes, bytes_to_long

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from psprofile.models import Account

class SalmonUserKey(models.Model):
    """
    Stores private keys for Salmon users on this machine
    """
    user = models.OneToOneField(User)
    private_key = models.TextField()
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    class Meta:
        verbose_name = "Salmon User Key"
        verbose_name_plural = "Salmon User Keys"

    def __str__(self):
        return self.user.username

    @staticmethod
    def get_new_private_key():
        """
        Generates a new private key and exports it as a PEM for storage in database
        """
        random_generator = Random.new().read
        pk = RSA.generate(1024, random_generator)
        return pk.exportKey('PEM')

    @staticmethod
    def get_private_key(user):
        """
        Returns a PEM string for a specific user so they can use it for signing
        """
        skey = SalmonUserKey.objects.get(user=user)
        return skey.private_key


class SalmonAccountKey(models.Model):
    """
    Stores public keys for Salmon users on this machine and remote users
    """
    account = models.OneToOneField(Account)
    public_key = models.TextField()

    class Meta:
        verbose_name = "Salmon Account Key"
        verbose_name_plural = "Salmon Account Keys"

    def __str__(self):
        return self.account.username + '@' + self.account.domain

    @staticmethod
    def get_public_key_from_private(private_key):
        """
        Takes a PEM private key string and returns a PEM public key
        """
        private_key = RSA.importKey(private_key)
        public_key = private_key.publickey()
        return public_key.exportKey('PEM')

    @staticmethod
    def get_magic_public_key(user):
        """
        Takes an account object, retrieves public key and returns the magic-public-key string for webfinger
        """
        acct_key = SalmonAccountKey.objects.get(account=user)
        public_key = RSA.importKey(acct_key.public_key)
        magic_key = 'data:application/magic-public-key,RSA.'
        magic_key += base64.urlsafe_b64encode(long_to_bytes(public_key.n)).decode('utf-8')
        magic_key += '.' + base64.urlsafe_b64encode(long_to_bytes(public_key.e)).decode('utf-8')
        return magic_key

    @staticmethod
    def get_public_key_from_magic(magic_key):
        start, enc_key, enc_exp = magic_key.split('.')
        bytes_key = base64.urlsafe_b64decode(enc_key)
        bytes_exp = base64.urlsafe_b64decode(enc_exp)
        public_key = RSA.construct((bytes_to_long(bytes_key), bytes_to_long(bytes_exp)))
        return public_key.exportKey('PEM')
