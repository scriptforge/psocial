from django.shortcuts import render

from pssite.models import Site

def host_meta(request):
    content_type = 'application/xrd+xml'
    context = {}
    context['site'] = Site.objects.all()[0]
    return render(request, 'salmon/salmon_host_meta.xml', context, content_type=content_type)

