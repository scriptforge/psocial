from django.contrib import admin
from salmon.models import *

admin.site.register(SalmonUserKey)
admin.site.register(SalmonAccountKey)
