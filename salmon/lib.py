from urllib import parse, request
import base64

from Crypto.Util.number import long_to_bytes
from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_PSS

import httpsig

from django.template.loader import get_template

from psprofile.models import Account
from salmon.models import SalmonAccountKey, SalmonUserKey
from pssite.models import Site
from notice.models import Notice

def activitystream_follow(account, remote_account, local_notice):
    """
    Sends an ActivityStream Atom Entry to remote site using Salmon Protocol
    to notify remote site about the follow
    """
    # Some constants we use here that never change
    data_type = 'application/atom+xml'
    sig_alg = 'RSA-SHA256'
    encoding = 'base64url'

    site = Site.objects.all()[0]

    # Generate the XML file to go into the Magic envelope
    msg_template = get_template('salmon/salmon_follow.xml')
    msg_context = {}
    msg_context['notice'] = local_notice
    msg_context['account'] = account
    msg_context['remote_account'] = remote_account
    msg_context['site'] = site
    message = msg_template.render(msg_context)

    # Generate the magic envelope
    magic_envelope_data = base64.urlsafe_b64encode(message.encode('utf-8'))

    msg_to_sign = magic_envelope_data + '.'.encode('utf-8') + base64.urlsafe_b64encode(data_type.encode('utf-8'))
    msg_to_sign += '.'.encode('utf-8') + base64.urlsafe_b64encode(encoding.encode('utf-8'))
    msg_to_sign += '.'.encode('utf-8') + base64.urlsafe_b64encode(sig_alg.encode('utf-8'))

    user_key = SalmonUserKey.get_private_key(account.user)
    signer = httpsig.Signer(secret=user_key, algorithm='rsa-sha256')
    magic_envelope_signature = signer._sign(msg_to_sign)

    env_template = get_template('salmon/salmon_magic_envelope.xml')
    env_context = {}
    env_context['magic_envelope_data'] = magic_envelope_data
    env_context['magic_envelope_signature'] = magic_envelope_signature
    envelope = env_template.render(env_context).encode('utf-8')

    # Send the envelope to the remote salmon url
    req = request.Request(remote_account.salmon_url)
    req.add_header('Content-type', 'application/magic-envelope+xml')
    with request.urlopen(req, envelope) as response:
        rpage = response.read()

def activitystream_unfollow(account, remote_account, local_notice):
    """
    Sends an ActivityStream Atom Entry to remote site using Salmon Protocol
    to notify remote site about the unfollow
    """

    # Some constants we use here that never change
    data_type = 'application/atom+xml'
    sig_alg = 'RSA-SHA256'
    encoding = 'base64url'

    site = Site.objects.all()[0]

    # Generate the XML file to go into the Magic envelope
    msg_template = get_template('salmon/salmon_unfollow.xml')
    msg_context = {}
    msg_context['notice'] = local_notice
    msg_context['account'] = account
    msg_context['remote_account'] = remote_account
    msg_context['site'] = site
    message = msg_template.render(msg_context)
    with open('/tmp/msg', 'w') as f:
        f.write(message)

    # Generate the magic envelope
    magic_envelope_data = base64.urlsafe_b64encode(message.encode('utf-8'))

    msg_to_sign = magic_envelope_data + '.'.encode('utf-8') + base64.urlsafe_b64encode(data_type.encode('utf-8'))
    msg_to_sign += '.'.encode('utf-8') + base64.urlsafe_b64encode(encoding.encode('utf-8'))
    msg_to_sign += '.'.encode('utf-8') + base64.urlsafe_b64encode(sig_alg.encode('utf-8'))

    user_key = SalmonUserKey.get_private_key(account.user)
    signer = httpsig.Signer(secret=user_key, algorithm='rsa-sha256')
    magic_envelope_signature = signer._sign(msg_to_sign)

    env_template = get_template('salmon/salmon_magic_envelope.xml')
    env_context = {}
    env_context['magic_envelope_data'] = magic_envelope_data
    env_context['magic_envelope_signature'] = magic_envelope_signature
    envelope = env_template.render(env_context).encode('utf-8')

    # Send the envelope to the remote salmon url
    req = request.Request(remote_account.salmon_url)
    req.add_header('Content-type', 'application/magic-envelope+xml')
    with request.urlopen(req, envelope) as response:
        rpage = response.read()
        print(rpage)
