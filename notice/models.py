from django.utils import timezone
from django.db import models

from psprofile.models import Account


class Notice(models.Model):
    profile = models.ForeignKey(Account)
    uri = models.CharField(max_length=191, blank=True)
    content = models.TextField(default='')
    rendered = models.TextField(default='')
    content_type = models.CharField(max_length=191)
    url = models.URLField(blank=True, null=True)
    reply_to = models.ForeignKey('self', null=True, related_name='notice_reply_to', blank=True)
    repeat_of = models.ForeignKey('self', null=True, related_name='notice_repeat_of', blank=True)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)
    mentions = models.ManyToManyField(Account, related_name='notice_account_mentions', blank=True)
    favourited_by = models.ManyToManyField(Account, related_name='notice_account_favourite', blank=True)
    repeated_by = models.ManyToManyField(Account, related_name='notice_account_repeat', blank=True)
    replies = models.ManyToManyField("Notice", related_name='notice_notice_replies', blank=True)

    class Meta:
        verbose_name = 'Notice'
        verbose_name_plural = 'Notices'

    def __str__(self):
        return self.profile.username + '@' + self.profile.domain + ':' + self.content
