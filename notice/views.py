from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse

from pssite.models import Site
from psprofile.models import Account
from notice.models import Notice
from notice.forms import NoticeForm


@require_http_methods(["POST"])
@login_required
def new_notice(request):
    """
    Post a new notice and then return to the previous page
    """
    site = Site.objects.all()[0]
    notice_form = NoticeForm(request.POST)

    next = request.GET.get('next', '')
    if next == '':
        next = '/'

    if notice_form.is_valid():
        # TODO Directed messages not currently working or useful yet.
        # Create a new notice
        account = Account.objects.get(username=request.user, domain=site.domain)
        # TODO Scan notice for a mention
        # TODO Apply a reply to field and handle it
        # TODO Strip rendering styles from content so bad HTML can't enter
        # TODO Apply rendering styles to the page (eg markdown, other content types)
        rendered = notice_form.cleaned_data['message']
        # Just a plain ordinary message
        created_notice = Notice.objects.create(profile=account,
                                  content=notice_form.cleaned_data['message'],
                                  rendered=rendered,
                                  content_type='psocial/notice')
        # TODO Add celery task to push the message to remote followers
        messages.success(request, 'Message posted.')
    else:
        messages.error(request, 'Notice form data is not valid')
        return HttpResponseRedirect(next)

    return HttpResponseRedirect(next)

def view_notice(request, notice_id=None):
    """
    View a single notice
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    # This is an anonymous view as well
    if request.user.is_authenticated():
        context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
        context['notice_form'] = NoticeForm()
        context['notice_form'].helper.form_action += '?next=' + reverse('view_notice', kwargs={'notice_id': notice_id})

    if notice_id is None:
        messages.error(request, 'No notice specified')
        return HttpResponseRedirect('home_view')

    notices = Notice.objects.filter(id=notice_id)
    if len(notices) == 0:
        messages.error(request, 'Notice Not Found')
        return HttpResponseRedirect(reverse('home_view'))

    context['page_profile'] = context['profile']
    context['notices'] = notices

    return render(request, "notice.html", context)

@login_required
def repeat_notice(request, notice_id=None):
    """
    Repeat a notice to your timeline.
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    next = request.GET.get('next', '/')
    try:
        orig_notice = Notice.objects.get(id=notice_id)
    except Notice.DoesNotExist:
        messages.error(request, 'Notice does not exist')
        return HttpResponseRedirect(next)

    orig_notice.repeated_by.add(context['profile'])
    new_notice = Notice.objects.create(profile=context['profile'],
                                       content_type='psocial/repeat',
                                       content=orig_notice.content,
                                       rendered=orig_notice.rendered,
                                       repeat_of=orig_notice)
    messages.success(request, 'Repeated message.')

    # TODO Ping remote source of repeat notification
    # TODO Ping remote followers of repeated notice

    return HttpResponseRedirect(next)

@require_http_methods(["GET", "POST"])
@login_required
def reply_to_notice(request, notice_id=None):
    """
    Post a new notice and then return to the previous page
    """
    site = Site.objects.all()[0]

    next = request.GET.get('next', '')

    if request.method == 'GET':
        if notice_id is None:
            messages.error(request, 'No message selected to reply to.')
            return HttpResponseRedirect(next)
        context = {}
        context['site'] = site
        context['profile'] = Account.objects.get(username=request.user.username, domain=site.domain)
        notices = Notice.objects.filter(id=notice_id)
        if len(notices) == 0:
            messages.error(request, 'Notice does not exist')
            return HttpResponseRedirect(next)
        context['notices'] = notices
        # Display form with reply_to filled in ready for user to enter message
        notice_form = NoticeForm(initial={'reply_to': notice_id})
        notice_form.helper.form_action = '/notice/reply?next=' + next
        context['notice_form'] = notice_form
        return render(request, 'reply.html', context)
    else:
        # POST method
        notice_form = NoticeForm(request.POST)

        if notice_form.is_valid():
            try:
                orig_notice = Notice.objects.get(id=notice_form.cleaned_data['reply_to'])
            except Notice.DoesNotExist:
                messages.error(request, 'Notice does not exist')
                return HttpResponseRedirect(next)

            # TODO Directed messages not currently working or useful yet.
            # Create a new notice
            account = Account.objects.get(username=request.user, domain=site.domain)
            # TODO Scan notice for a mention
            # TODO Apply a reply to field and handle it
            # TODO Strip rendering styles from content so bad HTML can't enter
            # TODO Apply rendering styles to the page (eg markdown, other content types)
            rendered = notice_form.cleaned_data['message']
            # Just a plain ordinary message
            created_notice = Notice.objects.create(profile=account,
                                      content=notice_form.cleaned_data['message'],
                                      rendered=rendered,
                                      reply_to=orig_notice,
                                      content_type='psocial/reply')
            # Update original notice with reply
            orig_notice.replies.add(created_notice)

            # TODO Add celery task to push the message to remote followers
            messages.success(request, 'Reply posted.')
        else:
            messages.error(request, 'Notice form data is not valid')
            return HttpResponseRedirect(next)

    return HttpResponseRedirect(next)


@login_required
def delete_notice(request, notice_id=None):
    """
    Repeat a notice to your timeline.
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    next = request.GET.get('next', '/')
    try:
        orig_notice = Notice.objects.get(id=notice_id)
    except Notice.DoesNotExist:
        messages.error(request, 'Notice does not exist')
        return HttpResponseRedirect(next)

    # Need to make sure this notice belongs to user
    # TODO or has permission can delete notices
    if orig_notice.profile != context['profile']:
        messages.error(request, 'You do not have permission to delete that notice')
        return HttpResponseRedirect(next)

    orig_notice.repeated_by.add(context['profile'])
    # Create a stub notice
    new_notice_content = context['profile'].name + ' deleted notice.'
    new_notice = Notice.objects.create(profile=context['profile'],
                                       content_type='psocial/delete',
                                       content=new_notice_content,
                                       rendered=new_notice_content,
                                       created_at=orig_notice.created_at,
                                       updated_at=orig_notice.updated_at)

    # Delete the old message, but first will need to update replies that reference the original

    # Update reference in the replies to now point to deleted stub notice
    for reply in orig_notice.replies.all():
        new_notice.replies.add(reply)
        reply.reply_to = new_notice
        reply.save()

    # TODO - Do we delete repeats of this?

    orig_notice.delete()
    new_notice.save()

    messages.success(request, 'Deleted message.')

    # TODO Ping remote source of repeat notification
    # TODO Ping remote followers of repeated notice
    # TODO Ping remote replies of deleted notice

    return HttpResponseRedirect(next)



@login_required
def favourite_notice(request, notice_id=None):
    """
    Favourite a notice
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    next = request.GET.get('next', '/')
    try:
        orig_notice = Notice.objects.get(id=notice_id)
    except Notice.DoesNotExist:
        messages.error(request, 'Notice does not exist')
        return HttpResponseRedirect(next)

    orig_notice.favourited_by.add(context['profile'])

    # Lookup original message from repeat and favourite that too
    if orig_notice.content_type == 'psocial/repeat':
        orig_notice.repeat_of.favourited_by.add(context['profile'])

    messages.success(request, 'Favourited message.')


    #TODO Ping remote source of favourite notification
    #TODO Ping original favourite of favourite notification (perhaps use signals?)

    return HttpResponseRedirect(next)


@login_required
def unfavourite_notice(request, notice_id=None):
    """
    Favourite a notice
    """
    context = {}
    context['site'] = Site.objects.all()[0]
    context['profile'] = Account.objects.get(username=request.user.username, domain=context['site'].domain)
    next = request.GET.get('next', '/')
    try:
        orig_notice = Notice.objects.get(id=notice_id)
    except Notice.DoesNotExist:
        messages.error(request, 'Notice does not exist')
        return HttpResponseRedirect(next)

    orig_notice.favourited_by.remove(context['profile'])

    # Lookup original message from repeat and unfavourite that too
    if orig_notice.content_type == 'psocial/repeat':
        orig_notice.repeat_of.favourited_by.remove(context['profile'])

    messages.success(request, 'Removed favourite.')

    #TODO Ping remote source of unfavourite notification
    #TODO Ping original favourite of unfavourite notification

    return HttpResponseRedirect(next)

