from django import forms
from django.core.urlresolvers import reverse


from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions


def get_sendto_choices():
    return ((1, "Everyone"),)


class NoticeForm(forms.Form):
    message = forms.CharField(
        label='Message',
        widget=forms.Textarea
    )

    send_to = forms.ChoiceField(
        choices=get_sendto_choices,
        label="Send to",
        widget=forms.Select
    )

    reply_to = forms.IntegerField(
        widget=forms.HiddenInput, required=False
    )

    def __init__(self, *args, **kwargs):
        super(NoticeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.form_action = reverse('new_notice')
        self.helper.form_id = 'new_notice_form'
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Field('message', css_class='input-xlarge', rows='3'),
            Field('send_to', css_class='input-xlarge'),
            Field('reply_to'),
            FormActions(
                Submit('post_message', 'Post Message', css_class='btn-primary')
            )
        )
