from django.conf.urls import patterns, url
from notice import views

urlpatterns = [
    url(r'^new$', views.new_notice, name='new_notice'),
    url(r'^repeat/(?P<notice_id>[0-9]+)$', views.repeat_notice, name='repeat_notice'),
    url(r'^delete/(?P<notice_id>[0-9]+)$', views.delete_notice, name='delete_notice'),
    url(r'^favourite/(?P<notice_id>[0-9]+)$', views.favourite_notice, name='favourite_notice'),
    url(r'^reply/(?P<notice_id>[0-9]+)$', views.reply_to_notice, name='reply_to_notice_form'),
    url(r'^reply$', views.reply_to_notice, name='reply_to_notice'),
    url(r'^unfavourite/(?P<notice_id>[0-9]+)$', views.unfavourite_notice, name='unfavourite_notice'),
    url(r'^(?P<notice_id>[0-9]+)$', views.view_notice, name='view_notice'),
]
