# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-03-26 11:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psprofile', '0011_auto_20160326_2153'),
        ('notice', '0004_auto_20160326_2158'),
    ]

    operations = [
        migrations.AddField(
            model_name='notice',
            name='favourited_by',
            field=models.ManyToManyField(blank=True, null=True, related_name='notice_account_favourited', to='psprofile.Account'),
        ),
        migrations.AddField(
            model_name='notice',
            name='mentions',
            field=models.ManyToManyField(blank=True, null=True, related_name='notice_account_mentions', to='psprofile.Account'),
        ),
    ]
